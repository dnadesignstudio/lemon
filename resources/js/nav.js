
$(document).ready(function(){

	var mob=$("#mob");// hidden nav div
	var but=$('#nav-icon3'); // // button to open

	var mt = $("#mainMenu ul").css('margin-top');

    but.click(function(){

        $(this).toggleClass('open');

if ($(this).hasClass("open")) {
	

mob.css('overflow','auto');

 mob.find("ul").eq(0).css({
        'margin-top': '100px',
        'opacity': '0'
      });
mob.fadeIn(400);
$("body").css('overflow','hidden');
mob.find("ul").eq(0).animate({
        'margin-top': "0px",
        'opacity': '1'
      }, 1000);


} else {	
mob.css('overflow','hidden');
$("body").css('overflow','auto');
mob.fadeOut(600,function(){
	

});

  mob.find("ul").eq(0).animate({
        'margin-top': '100px',
        'opacity': '0'
      }, 500);
}

        //mob.toggle( "easing");
 /*
        mob.slideToggle(600, 'swing', function() {
           //callback function after animation finished
        
          });*/
    });
});


/////////////////////////////////////////////////////////////////

//scrollTo

// <a data-scrollTo="#target">go to target</a>
// <div id="target"></div>

(function() {
  $(document).ready(function() {
    var fis = $('[data-scrollTo]');
    console.log(fis.length+" st");
    fis.click(scto);
  });

  function scto(ev) {
    if(ev)ev.preventDefault();
    var y;
    var ee=$(this).attr('data-scrollTo');
    if(ee=="TOP"){y=0;} else {
    var el = $(ee);
     y = $(el[0]).offset().top;
    }
    console.log(y);
    
    $('html, body').animate({
      'scrollTop': y+"px"
    }, 600, 'easeInOutQuad');
  }
})();



/////////////////////////////////////////////////////////////////



// fade elements
(function() {
  var margin = 50;
  $(document).ready(init);

  function init() {

    var fis = $('.fadeDiv img');
    $('.fadeDiv').css('opacity','0');
    if (fis.length > 0) {
      var fi = $(fis[0]).attr('src');
      $('body').prepend('<img id="preloadHeadImage" data-src="' + fi + '" style="position:absolute;left:-5000;"/>');
    }


    fi = $('.fadeDiv');

    var pli = $('#preloadHeadImage');
    if (pli.length > 0) {
      $('#preloadHeadImage').css({
        'position': 'absolute',
        'top': '-5000px'
      });
      preloadImage();
    } else {
      showNext();
    }

  }

  function preloadImage() {
    var pli = $('#preloadHeadImage');
    if (pli.length > 0) {
      var src = pli.attr('data-src');
      pli.load(iLoaded);
      pli.attr('src', src);
    } else {
      showNext()
    }
  }

  function iLoaded() {

    showNext();
    $('#preloadHeadImage').remove();
  }

  function showNext() {
    if ($('.fadeDiv').length < 1) return;

    var ad = $($('.fadeDiv')[0]);
    //ad.show();
    if (!getScrollLineVisible(ad, showNext)) {
      $(window).scroll(onscroll);
      return;
    }

    //ad.removeClass('fadeDiv');
    var mt = ad.css('margin-top');
    ad.css('opacity', '0');
  ad.css('margin-top','40px');
    ad.animate({
      opacity: 1,
      'margin-top': mt
    }, 300, function() {
      $($('.fadeDiv')[0]).removeClass('fadeDiv');
      showNext()
    });



  }
  var $firsthide = null;
  var nfn;

  function getScrollLineVisible(el, fn) {

    $firsthide = el;
    nfn = fn;
    return ((el.offset().top + margin) - $(window).height() < $(window).scrollTop())
  }

  function onscroll() {
    //($(window).width()<1020)?-50:70;
    if (($firsthide.offset().top + margin) - $(window).height() < $(window).scrollTop()) {
      $(window).unbind('scroll', onscroll);
      nfn();
    }
  }

  function initF() {
    var fi = $('.fadeDiv');
    fi.show();
    showNext();
  }

  window.initFades = initF;

})();




jQuery.easing.jswing=jQuery.easing.swing;
jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,f,a,h,g){return jQuery.easing[jQuery.easing.def](e,f,a,h,g)},easeInQuad:function(e,f,a,h,g){return h*(f/=g)*f+a},easeOutQuad:function(e,f,a,h,g){return -h*(f/=g)*(f-2)+a},easeInOutQuad:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f+a}return -h/2*((--f)*(f-2)-1)+a},easeInCubic:function(e,f,a,h,g){return h*(f/=g)*f*f+a},easeOutCubic:function(e,f,a,h,g){return h*((f=f/g-1)*f*f+1)+a},easeInOutCubic:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f+a}return h/2*((f-=2)*f*f+2)+a},easeInQuart:function(e,f,a,h,g){return h*(f/=g)*f*f*f+a},easeOutQuart:function(e,f,a,h,g){return -h*((f=f/g-1)*f*f*f-1)+a},easeInOutQuart:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f+a}return -h/2*((f-=2)*f*f*f-2)+a},easeInQuint:function(e,f,a,h,g){return h*(f/=g)*f*f*f*f+a},easeOutQuint:function(e,f,a,h,g){return h*((f=f/g-1)*f*f*f*f+1)+a},easeInOutQuint:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f*f+a}return h/2*((f-=2)*f*f*f*f+2)+a},easeInSine:function(e,f,a,h,g){return -h*Math.cos(f/g*(Math.PI/2))+h+a},easeOutSine:function(e,f,a,h,g){return h*Math.sin(f/g*(Math.PI/2))+a},easeInOutSine:function(e,f,a,h,g){return -h/2*(Math.cos(Math.PI*f/g)-1)+a},easeInExpo:function(e,f,a,h,g){return(f==0)?a:h*Math.pow(2,10*(f/g-1))+a},easeOutExpo:function(e,f,a,h,g){return(f==g)?a+h:h*(-Math.pow(2,-10*f/g)+1)+a},easeInOutExpo:function(e,f,a,h,g){if(f==0){return a}if(f==g){return a+h}if((f/=g/2)<1){return h/2*Math.pow(2,10*(f-1))+a}return h/2*(-Math.pow(2,-10*--f)+2)+a},easeInCirc:function(e,f,a,h,g){return -h*(Math.sqrt(1-(f/=g)*f)-1)+a},easeOutCirc:function(e,f,a,h,g){return h*Math.sqrt(1-(f=f/g-1)*f)+a},easeInOutCirc:function(e,f,a,h,g){if((f/=g/2)<1){return -h/2*(Math.sqrt(1-f*f)-1)+a}return h/2*(Math.sqrt(1-(f-=2)*f)+1)+a},easeInElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return -(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e},easeOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return g*Math.pow(2,-10*h)*Math.sin((h*k-i)*(2*Math.PI)/j)+l+e},easeInOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k/2)==2){return e+l}if(!j){j=k*(0.3*1.5)}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}if(h<1){return -0.5*(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e}return g*Math.pow(2,-10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j)*0.5+l+e},easeInBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*(f/=h)*f*((g+1)*f-g)+a},easeOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*((f=f/h-1)*f*((g+1)*f+g)+1)+a},easeInOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}if((f/=h/2)<1){return i/2*(f*f*(((g*=(1.525))+1)*f-g))+a}return i/2*((f-=2)*f*(((g*=(1.525))+1)*f+g)+2)+a},easeInBounce:function(e,f,a,h,g){return h-jQuery.easing.easeOutBounce(e,g-f,0,h,g)+a},easeOutBounce:function(e,f,a,h,g){if((f/=g)<(1/2.75)){return h*(7.5625*f*f)+a}else{if(f<(2/2.75)){return h*(7.5625*(f-=(1.5/2.75))*f+0.75)+a}else{if(f<(2.5/2.75)){return h*(7.5625*(f-=(2.25/2.75))*f+0.9375)+a}else{return h*(7.5625*(f-=(2.625/2.75))*f+0.984375)+a}}}},easeInOutBounce:function(e,f,a,h,g){if(f<g/2){return jQuery.easing.easeInBounce(e,f*2,0,h,g)*0.5+a}return jQuery.easing.easeOutBounce(e,f*2-g,0,h,g)*0.5+h*0.5+a}});
