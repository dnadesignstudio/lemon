 (function(){
var ISV=false;
var index=0;
var ims=[];
var px=0;
var il=0;
var npp=10;
var max=10;
var $imgcontainer;
$(document).ready(init);

function init(){
$imgcontainer   = $('#am-container');

var          $imgs   = $imgcontainer.find('img'),
          totalImgs = $imgs.length,
          cnt     = 0;
$imgs.each(function(){ims.push($(this).clone())});

$('#am-container').css('width','200%');
$('#am-container').parent().css('overflow','hidden');

$imgcontainer.html('');
$imgcontainer.show();
 

loadNext();
$(window).resize(reload);
$($('.whitebtn[href=#]')).addClass('loadMoreButton');

$('.loadMoreButton').click(function(e){e.preventDefault();loadMore()});
$('.loadMoreButton').hide();
}


function reload(){
  il=0;  px=0;
$('#am-container').html('');
loadNext();
 closeFull();
} 

function loadNext(){
    //console.log(ims.length+" "+il);
if(il>= ims.length){
   $('.loadMoreButton').hide();
  return;
}

if(il>=max){
 if(px>=$(window).width()){

   //console.log("px > width??");
 $('.loadMoreButton').show();
  return;
 }
}

if(px>$(window).width()){ 
px=0;
   //console.log("px > width?? -> br");
$imgcontainer.append( "<br/>" ); 
}

var im=$(ims[il]).clone();
var src=im.attr('src');
var im1=$("<img src=''/>");//im.attr('src','');
   //console.log(src);
 $imgcontainer.append(im1);
im1.load(iLoaded);
im1.attr('src',src);
}

function iLoaded(){
     //console.log("il "+il);
$(this).css({'opacity':'0','cursor':'pointer','width':'auto','height':'270px','position':'relative'});
$(this).show();

$(this).attr('data-id',il);
$(this).click(openImage);

$(this).animate({opacity:1},400,iReady);
}

function iReady(){
  px+=$(this).width();//position().left;//width()
  

il++;
   //console.log("px "+px+ " il: "+il);

loadNext();

}
 

 function loadMore(){
max+=npp;

loadNext();
 }



//////////////////////// FULLSCREEN IMG
var full;
 function openImage(){

  $('#FULLIMAGE').html('');
$('#FULLIMAGE').remove();

var src=$(this).attr('src');

index=$(this).attr('data-id');

var full=$("<div  class='grayBg'  id='FULLIMAGE'><div style='' id='FullBg'></div></div>");

var close=$("<div id='fullCloseBut' style='position:absolute;top:20px;right:20px;cursor:pointer;'><a id='xclose' href='javascript:closeFull()' title='Close'><span>Close image</span></a></div>");

var prev=$("<div style='cursor:pointer;' class='larrow' ></div>");
var next=$("<div style='cursor:pointer;' class='rarrow' ></div>");

 


var ima=$("<img style='position:absolute;opacity:0;' />");
var v=$(this).attr('data-video');
 ISV=(v && v!='' && v!='null' && v!='no' );

$('body').append(full);
 full.append(ima);

if(!ISV){
ima.load(fullLoaded);
ima.attr('src',src);
} else {


$("<div id='VIDEO'></div>" ).insertAfter(ima);
ima.hide();
buildVideo(v);
}

full.append(prev);
full.append(next);

 //full.append(close);

 prev.click(function(){prevFull()});
 next.click(function(){nextFull()});
//close.click(function(){me.close()});
 $('#FullBg').click(function(){closeFull()});

$('body').keydown(KD);


}


function KD(e) {

if(e.keyCode==39 || e.keyCode== 40){ nextFull()};
if(e.keyCode==37 || e.keyCode== 38){ prevFull()};



}

function fullLoaded(){

var w=$(this).width();
var h=$(this).height();

if(h>$(window).height()){
$(this).css({'height':$(window).height()+'px','width':'auto'});  
}

w=$(this).width();
h=$(this).height();


if(w>$(window).width()){
$(this).css({'width':$(window).width()+'px','height':'auto'});  
}
 w=$(this).width();
 h=$(this).height();


var x=($(window).width()/2)-(w/2);
var y=($(window).height()/2)-(h/2);

$(this).css({'left':(x)+'px','top':(y)+'px'});

$(this).click(function(){nextFull()});
$('#fullCloseBut').css({'left':(x+(w-70))+'px','top':(y )+'px'});
$(this).animate({opacity:1},400);

}


function prevFull(){
index--;
if(index<0){
  index=ims.length-1;

}
 

showActual();

}


function nextFull(){

index++;
 
 if( index>=ims.length){
  index=0;
}
 
showActual();

} 


function showActual(){

var src=$(ims[index]).attr('src');

 $($("#FULLIMAGE").find('img')[0]).remove();
 if(ISV){
$("#VIDEO" ).html('');
$("#VIDEO" ).remove();
}
var im=$("<img style='position:absolute;opacity:0;' />");

var v=$(ims[index]).attr('data-video');
 ISV=(v && v!='' && v!='null' && v!='no' );


$("#FULLIMAGE").prepend(im);
$("#FULLIMAGE").prepend($('#FullBg'));
 

if(!ISV){
im.attr('src','');
im.load(fullLoaded);
im.attr('src',src);
} else {
$("<div id='VIDEO'></div>" ).insertAfter(im);
im.hide();
buildVideo(v);
}


}


function buildVideo(v){
$('#VIDEO').css('position','absolute');
$('#VIDEO').css('width','640px');
$('#VIDEO').css('height','390px');
$('#VIDEO').css('left',(($(window).width()/2)-(640/2))+'px');
$('#VIDEO').css('top',(($(window).height()/2)-(390/2))+'px');
$('#VIDEO').css({'overflow':'hidden','margin':'0px','padding':'0px','background-color':'#000000'});
$('#fullCloseBut').css({'right':'20px','top':(20 )+'px'});

var vn=v.split('?')[1];
var varr=vn.split('&');
var code='';
 for(var i=0;i<varr.length;i++){
  if(varr[i].split('=')[0]=='v'){
    code=varr[i].split('=')[1];
  }
}

if(code!=''){
$('#VIDEO').html('<iframe style="background-color:#000000;border:none;padding:0px;background-color:#000000;margin:0px;" width="640" height="390" src="http://www.youtube.com/embed/'+code+'" frameborder="0"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
return;
}


var  vna=v.split('vimeo.com/');
if(vna.length>0) {
  code=vna[vna.length-1];



$('#VIDEO').html('<iframe style="background-color:#000000;border:none;padding:0px;background-color:#000000;margin:0px;" width="640" height="390"  src="//player.vimeo.com/video/'+code+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
}


}


function closeFull(){
 if(ISV){
  $('#VIDEO').animate({opacity:0},500,function(){
$("#VIDEO" ).html('');
$("#VIDEO" ).remove();
});
ISV=false;
}



$('body').unbind('keydown',KD);

$('#FULLIMAGE').animate({opacity:0},500,function(){
$('#FULLIMAGE').html('');
$('#FULLIMAGE').remove();
});
 
}
window.closeFull=closeFull;

})();


/*

  <div class="am-container" id="am-container">

          {% if galleryItems|length > 0 %}
              {% for i in galleryItems %}
                <img title="{{ i['title'] }}" src="{{ i['image'] }}" {% if i['video'] %}data-video="{{ i['video'] }}"{% endif %} ></img>
              {% endfor %}
          {% endif %}

      </div>



*/